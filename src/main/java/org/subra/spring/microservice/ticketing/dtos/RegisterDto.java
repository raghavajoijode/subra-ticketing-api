package org.subra.spring.microservice.ticketing.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegisterDto {

	@NotNull(message = "`email` field is mandatory")
	@Email(message = "`email` must be an well-formed email address")
	private String email;
	
	@NotNull(message = "`password` field is mandatory")
	@Size(min = 8, message = "`password` must be at least 8 characters long")
	private String password;
	private String name;
}
