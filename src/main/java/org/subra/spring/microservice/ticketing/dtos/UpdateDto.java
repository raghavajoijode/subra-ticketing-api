package org.subra.spring.microservice.ticketing.dtos;

import org.subra.spring.microservice.ticketing.documents.Update;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class UpdateDto extends Update{
	private static final long serialVersionUID = 1L;
	private long id;
}
