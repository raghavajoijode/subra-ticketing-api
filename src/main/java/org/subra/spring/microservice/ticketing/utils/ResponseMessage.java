package org.subra.spring.microservice.ticketing.utils;

public enum ResponseMessage {
	EMPTY_REPOSITORY, TICKET_NOT_FOUND, TICKET_ALREADY_AVAILABLE, OPERATION_EXECUTED;
}
